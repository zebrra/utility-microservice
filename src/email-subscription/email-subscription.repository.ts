import { InternalServerErrorException, Logger } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { EmailSubscriptionDto } from "./email-subscription.dto";
import { EmailSubscription } from "./email-subscription.entity";

@EntityRepository(EmailSubscription)
export class EmailSubscriptionRepository extends Repository<EmailSubscription> {
  private logger = new Logger('EmailSubscriptionRepository');

  async addEmail(emailSubscriptionDto: EmailSubscriptionDto): Promise<EmailSubscription> {
    const { email } = emailSubscriptionDto;

    const emailSubscription = new EmailSubscription();
    emailSubscription.email = email;

    try {
      await emailSubscription.save();
    } catch (error) {
      this.logger.error(
        `Failed to add email "${emailSubscription.email}" to email subscription list. Data: ${emailSubscriptionDto}, error.stack`
      )
      throw new InternalServerErrorException();
    } 
      return emailSubscription;
  }
  
  }
