import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { EmailSubscriptionDto } from "./email-subscription.dto";
import { EmailSubscription } from "./email-subscription.entity";
import { EmailSubscriptionRepository } from "./email-subscription.repository";
import { MailService } from "src/mail/mail.service";

@Injectable()
export class EmailSubscriptionService {
  constructor(
    @InjectRepository(EmailSubscriptionRepository)
    private emailSubscriptionRepository: EmailSubscriptionRepository, private mailService: MailService
  ) { }
  
  async addEmail(emailSubscriptionDto: EmailSubscriptionDto): Promise<EmailSubscription> {
    const addedEmail = await this.emailSubscriptionRepository.addEmail(
      emailSubscriptionDto
    );
    await this.mailService.sendSubcriptionMail(emailSubscriptionDto.email)
    return addedEmail;
  } 
}
