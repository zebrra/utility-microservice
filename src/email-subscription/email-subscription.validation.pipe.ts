import { PipeTransform, Injectable, BadRequestException } from '@nestjs/common';

@Injectable()
export class EmailValidationPipe implements PipeTransform {
  transform(value: any) {
    if(value.email){
      value.email = value.email.trim();
    }

    if (!this.validateEmail(value.email)) {
      throw new BadRequestException(`"${value.email} is an invalid email address"`)
    }
    return value;
  }

  private validateEmail(email: any) {
    const validRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email.match(validRegex)) {
      return email;
    }
  }
}
