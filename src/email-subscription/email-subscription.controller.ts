import { Body, Controller, Logger, Post, UsePipes, ValidationPipe } from "@nestjs/common";
import { EmailSubscriptionDto } from "./email-subscription.dto";
import { EmailSubscription } from "./email-subscription.entity";
import { EmailSubscriptionService } from "./email-subscription.service";
import { EmailValidationPipe } from "./email-subscription.validation.pipe";

@Controller('email-subscriptions')
export class EmailSubscriptionController {
  private logger = new Logger('EmailSubscriptionController');
  constructor(private emailSubscriptionService: EmailSubscriptionService) {}
  
  @Post()
  @UsePipes(ValidationPipe)
  addEmail(@Body(EmailValidationPipe) newEmailSubscriptionDto: EmailSubscriptionDto): Promise<EmailSubscription> {

    this.logger.verbose(
      `A new email address is being added to the email subscription list. Data: ${JSON.stringify(
        newEmailSubscriptionDto,
      )}`
    )
    return this.emailSubscriptionService.addEmail(newEmailSubscriptionDto)
  }
}