import { IsNotEmpty } from 'class-validator';

export class EmailSubscriptionDto {
  @IsNotEmpty()
  email: string;
}
