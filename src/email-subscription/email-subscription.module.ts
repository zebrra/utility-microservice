import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailSubscriptionController } from './email-subscription.controller';
import { EmailSubscriptionRepository } from './email-subscription.repository';
import { EmailSubscriptionService } from './email-subscription.service';
import { MailModule } from 'src/mail/mail.module';

@Module({
  imports: [TypeOrmModule.forFeature([EmailSubscriptionRepository]), MailModule],
  controllers: [EmailSubscriptionController],
  providers: [EmailSubscriptionService],
})
export class EmailSubscriptionModule {}
