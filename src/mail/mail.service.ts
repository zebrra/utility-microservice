import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) { }

  async sendSubcriptionMail(email: string) {
    await this.mailerService.sendMail({
      to: email,
      subject: 'Subscription to Zebrra Newsletter',
      template: 'subscription',
      context: {
        email: email
      }
    })
    console.log(email)
    console.log('it gets to the mailservice.sendSubcriptionMail')
  }

}
